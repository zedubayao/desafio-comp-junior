import { Router } from 'express';
import Project from '@/app/schemas/Project';
import Slugify from '@/utils/Slugify';

const router = new Router();

router.get('/', (req, res) => {
    Project.find().then(data => {
        const projects = data.map(project => {
            return {title: project.title, category: project.category};
        });
        return res.send(projects);
    })
    .catch(error => {
        console.error('Erro ao obter novo projeto no banco de dados', error);
        return res.status(400).send({error: 'Não foi possível obter seu projeto verifique os dados e tente novamente',});
    });
});
router.get('/:projectSlug', (req, res) => {
    Project.findOne({ slug: req.params.projectSlug })
    .then(project => {
        return res.send(project);
    })
    .catch(error => {
        console.error('Erro ao obter novo projeto no abnco de dados', error);
        return res.status(400).send({error: 'Não foi possível obter seu projeto verifique os dados e tente novamente',});
    });
});

router.post('/', (req, res) => {
    const {title, description, category} = req.body;
    let slug = undefined;
    if(title){
        slug = Slugify(title);
    }
    Project.create({title, slug, description, category})
        .then(project => {
            return res.status(200).send(project);
        })
        .catch(error => {
            console.error('Erro ao salvar novo projeto no abnco de dados', error);
            return res.status(400).send({error: 'Não foi possível salvar seu projeto verifique os dados e tente novamente',});
        });   
});

router.put('/:projectId', (req, res) => {
    const {title, description, category} = req.body;
    let slug = undefined;
    if(title){
        slug = Slugify(title);
    }
    Project.findByIdAndUpdate(req.params.projectId, 
        {title, slug, description, category}, 
        {new: true},
        )
        .then(project => {
            return res.status(200).send(project);
        })
        .catch(error => {
            console.error('Erro ao editar novo projeto no abnco de dados', error);
            return res.status(400).send({error: 'Não foi possível editar seu projeto verifique os dados e tente novamente',});
        });
});

router.delete('/:projectId', (req, res) => {
    Project.findByIdAndRemove(req.params.projectId)
    .then(() => {
        return res.send({message: 'Projeto removido com sucesso!'});
    }).catch(error => {
        console.error('Erro ao remover projeto do banco de dados' , error);
        return res
        .status(400)
        .send({message: 'Erro ao remover projeto, tente novamente!'});
    })
});

export default router;
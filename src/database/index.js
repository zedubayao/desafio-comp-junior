import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/portifolio-pessoal', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then(() => {
    console.log('Servidor MongoDB iniciado com sucesso!');
  })
  .catch((err) => {
    console.error(`Ocorreu o seguinte erro ao inicializar o servidor: ${err}`);
  });
mongoose.Promise = global.Promise;

export default mongoose;